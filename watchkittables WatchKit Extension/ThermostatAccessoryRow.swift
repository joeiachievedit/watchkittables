//
//  ThermostatAccessoryRow.swift
//  watchkittables
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

import Foundation
import WatchKit

class ThermostatAccessoryRow : NSObject {
  
  @IBOutlet weak var setpointTemperature: WKInterfaceLabel!

  var setpoint = 72
  
  @IBAction func setpointDown() {
    self.setpoint = self.setpoint - 1
    self.setpointTemperature.setText("\(self.setpoint)")
  }
  
  @IBAction func setpointUp() {
    self.setpoint = self.setpoint + 1
    self.setpointTemperature.setText("\(self.setpoint)")
  }
  
}
