//
//  LightbulbAccessoryRow.swift
//  watchkittables
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

import Foundation
import WatchKit

class LightbulbAccessoryRow : NSObject {
  
  @IBOutlet weak var lightbulbImage: WKInterfaceImage!
  @IBOutlet weak var lightbulbButton: WKInterfaceButton!
  
  var on:Bool = false
  
  @IBAction func buttonTapped() {
    if self.on {
      // Turn bulb off
      self.lightbulbImage.setImageNamed("off")
      self.lightbulbButton.setTitle("Turn On")
      self.on = false
    } else {
      // Turn bulb on
      self.lightbulbImage.setImageNamed("on")
      self.lightbulbButton.setTitle("Turn Off")
      self.on = true
    }
  }
  
  
}
