//
//  InterfaceController.swift
//  watchkittables WatchKit Extension
//
//  Created by Joseph Bell on 12/6/14.
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

  @IBOutlet weak var accessoryTable: WKInterfaceTable!
  
  override init(context: AnyObject?) {
    
    super.init(context: context)
    
    let accessories = [
      ["type":"lightbulb",
        "name":"Living Room",
        "state":"off"],
      ["type":"lightbulb",
        "name":"Dining Room",
        "state":"on"],
      ["type":"thermostat",
        "name":"Upstairs",
        "mode":"cool",
        "setpoint":"72"],
      ["type":"thermostat",
        "name":"Downstairs",
        "mode":"cool",
        "setpoint":"68"]
    ]
    
    var rowTypes = [String]()
    
    for accessory in accessories {
      var type = accessory["type"]!
      type = type.capitalizedString
      rowTypes.append("\(type)AccessoryRow")
    }
    
    self.accessoryTable.setRowTypes(rowTypes)
    
    for var i = 0; i < accessories.count; i++ {
      if accessories[i]["type"] == "lightbulb" {
        let row = self.accessoryTable.rowControllerAtIndex(i) as LightbulbAccessoryRow
        
        row.lightbulbImage.setImageNamed(accessories[i]["state"])
        if accessories[i]["state"] == "off" {
          row.on = false
          row.lightbulbButton.setTitle("Turn On")
        } else {
          row.on = true
          row.lightbulbButton.setTitle("Turn Off")
        }
        
      } else if accessories[i]["type"] == "thermostat" {
        let row = self.accessoryTable.rowControllerAtIndex(i) as ThermostatAccessoryRow
        let t = accessories[i]["setpoint"]
        
        row.setpointTemperature.setText(t)
        row.setpoint = t!.toInt()!
      }
    }
    
    
  }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }

}
